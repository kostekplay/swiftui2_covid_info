////  GettingTestApi.swift
//  SwiftUI2_CovidTotalLast
//
//  Created on 03/02/2021.
//  
//

import SwiftUI

func getTestConnectionToApi() {
    
    let headers = [
        "x-rapidapi-key": "1df314083cmsh003ec54127f765dp14fc5ejsnb06e31a1acf0",
        "x-rapidapi-host": "covid-19-data.p.rapidapi.com"
    ]
    
    let request = NSMutableURLRequest(url: NSURL(string: "https://covid-19-data.p.rapidapi.com/totals")! as URL,
                                      cachePolicy: .useProtocolCachePolicy,
                                      timeoutInterval: 10.0)
    request.httpMethod = "GET"
    request.allHTTPHeaderFields = headers
    
    let session = URLSession.shared
    let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
        if (error != nil) {
            print(error ?? "No Errors")
        } else {
            let httpResponse = response as? HTTPURLResponse
            print(httpResponse ?? "No HttpResponse")
        }
    })
    
    dataTask.resume()
}
