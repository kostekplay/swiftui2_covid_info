////  GettingInfo.swift
//  SwiftUI2_CovidTotalLast
//
//  Created on 03/02/2021.
//  
//

import SwiftUI

func getCovidDataHeaderV2(url: String, completion: @escaping([CovidGlobalLast]) -> ()){
    
    let headers = [
        "x-rapidapi-key": "1df314083cmsh003ec54127f765dp14fc5ejsnb06e31a1acf0",
        "x-rapidapi-host": "covid-19-data.p.rapidapi.com"
    ]
    let request = NSMutableURLRequest(url: NSURL(string: url)! as URL,
                                      cachePolicy: .useProtocolCachePolicy,
                                      timeoutInterval: 10.0)
    request.httpMethod = "GET"
    request.allHTTPHeaderFields = headers
    
    
    let session = URLSession(configuration: .default)
    session.dataTask(with: request as URLRequest) { (data, _, err) in
        if err != nil {
            print(err!.localizedDescription)
            return
        }
        do {
            let total = try JSONDecoder().decode([CovidGlobalLast].self, from: data!)
            completion(total)
        }
        catch {
            print(error)
        }
    }.resume()
}


// funkcja powinna być w strukturze z zadeklarowaną zmienną users

//func getCovidDataHeaderV1() {
//
//    let headers = [
//        "x-rapidapi-key": "1df314083cmsh003ec54127f765dp14fc5ejsnb06e31a1acf0",
//        "x-rapidapi-host": "covid-19-data.p.rapidapi.com"
//    ]
//    let request = NSMutableURLRequest(url: NSURL(string: "https://covid-19-data.p.rapidapi.com/totals")! as URL,
//                                      cachePolicy: .useProtocolCachePolicy,
//                                      timeoutInterval: 10.0)
//    request.httpMethod = "GET"
//    request.allHTTPHeaderFields = headers
//
//    let session = URLSession(configuration: .default)
//    session.dataTask(with: request as URLRequest) { (data, _, err) in
//        if err != nil {
//            print(err!.localizedDescription)
//            return
//        }
//        do {
//            let users = try JSONDecoder().decode([CovidGlobalLast].self, from: data!)
//            print(users)
//            self.covidDetal = users
//            return
//        }
//        catch {
//            print(error)
//        }
//    }.resume()
//}
