////  ContentView.swift
//  SwiftUI2_CovidTotalLast
//
//  Created on 02/02/2021.
//  
//

import SwiftUI



struct GlobalView: View {
    
    @State private var covidDetal: [CovidGlobalLast] = []
    @State var isEmpty = false
    
    var body: some View {
        NavigationView {
            VStack {
                GroupBox{
                    if covidDetal.isEmpty {
                        
                        Spacer()
                        ProgressView()
                        Spacer()
                        
                    } else {
                        Spacer()
                        
                        GlobalRowView(labelName: "Confirmed", labelImage: "cross.case", number: covidDetal.first!.confirmed)
                        
                        Spacer()
                        
                        GlobalRowView(labelName: "Death", labelImage: "hand.thumbsdown", number: covidDetal.first!.deaths)
                        
                        Spacer()
                        
                        GlobalRowView(labelName: "Recovered", labelImage: "hand.thumbsup", number: covidDetal.first!.recovered)
                        
                        Spacer()
                        
                        VStack {
                            VStack(alignment: .center, spacing: 4) {
                                Text("Last Change:")
                                Text(covidDetal.first!.lastChange)
                            }.font(.subheadline)
                            
                            Divider().padding(.vertical, 4)
                        }
                        Spacer()
                    }
                }.padding(.horizontal, 12)
            }
            .navigationTitle("Covid 19 data")
            .navigationBarItems(trailing: Button(action: {
                getCovidDataHeaderV2(url: "https://covid-19-data.p.rapidapi.com/totals") { (total) in
                    self.covidDetal = total
                }
            }, label: {
                Image(systemName: "arrow.clockwise")
            }))
        }
        .onAppear{
            getCovidDataHeaderV2(url: "https://covid-19-data.p.rapidapi.com/totals") { (total) in
                self.covidDetal = total
            }
            
        }
    }
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        GlobalView()
    }
}


//let headers = [
//    "x-rapidapi-key": "1df314083cmsh003ec54127f765dp14fc5ejsnb06e31a1acf0",
//    "x-rapidapi-host": "covid-19-data.p.rapidapi.com"
//]
//
//let request = NSMutableURLRequest(url: NSURL(string: "https://covid-19-data.p.rapidapi.com/country?name=italy")! as URL,
//                                        cachePolicy: .useProtocolCachePolicy,
//                                    timeoutInterval: 10.0)
//request.httpMethod = "GET"
//request.allHTTPHeaderFields = headers
//
//let session = URLSession.shared
//let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
//    if (error != nil) {
//        print(error)
//    } else {
//        let httpResponse = response as? HTTPURLResponse
//        print(httpResponse)
//    }
//})
//
//dataTask.resume()
