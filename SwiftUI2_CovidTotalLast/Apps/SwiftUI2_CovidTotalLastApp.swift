////  SwiftUI2_CovidTotalLastApp.swift
//  SwiftUI2_CovidTotalLast
//
//  Created on 02/02/2021.
//  
//

import SwiftUI

@main
struct SwiftUI2_CovidTotalLastApp: App {
    var body: some Scene {
        WindowGroup {
            MainView()
        }
    }
}
