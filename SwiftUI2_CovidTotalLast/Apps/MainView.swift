////  MainView.swift
//  SwiftUI2_CovidTotalLast
//
//  Created on 03/02/2021.
//  
//

import SwiftUI

struct MainView: View {
    var body: some View {
        TabView {
            
            GlobalView()
                .tabItem {
                    Image(systemName: "globe")
                    Text("Global info.")
                }
            
            CountryView()
                .tabItem {
                    Image(systemName: "flag")
                    Text("Country info.")
                }
        }
    }
}

struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        MainView()
    }
}
