////  GlobalRowView.swift
//  SwiftUI2_CovidTotalLast
//
//  Created on 03/02/2021.
//  
//

import SwiftUI

struct GlobalRowView: View {
    
    var labelName: String
    var labelImage: String
    var number: Int
    
    var body: some View {
        VStack {

            HStack(alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/, spacing: 12, content: {
                Image(systemName: labelImage)
                Text("\(labelName):")
            })
            .font(.title2)
            Text("\(number) people")
            .fontWeight(.bold)
            Divider().padding(.vertical, 4)
        }
    }
}

struct GlobalRowView_Previews: PreviewProvider {
    static var previews: some View {
        GlobalRowView(labelName: "Recovered", labelImage: "cross.case", number: 1232323)
            .previewLayout(.sizeThatFits)
            .padding()
    }
}
