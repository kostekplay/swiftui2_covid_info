////  CovidTotal.swift
//  SwiftUI2_CovidTotalLast
//
//  Created on 02/02/2021.
//  
//

import SwiftUI

struct CovidGlobalLast: Decodable {
    
    let confirmed: Int
    let critical: Int
    let deaths: Int
    let lastChange: String
    let lastUpdate: String
    let recovered: Int
    
}



//[
//    {
//    "confirmed":15270765
//    "critical":63634
//    "deaths":624093
//    "lastChange":"2020-07-22T19:54:49+02:00"
//    "lastUpdate":"2020-07-22T20:00:04+02:00"
//    "recovered":9207832
//    }
//]
